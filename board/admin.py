from django.contrib import admin
from models import *

class AdImageInline(admin.TabularInline):
  model= AdImage  


class CategoryAdmin(admin.ModelAdmin):
  save_on_top = True
  exclude = ["slug"]
  list_display = ('id', 'name' , 'slug')

class SubcategoryAdmin(admin.ModelAdmin):
  save_on_top = True
  exclude = ["slug"]
  list_display = ('id', 'name', 'category')

class AdvertisementAdmin(admin.ModelAdmin):
  save_on_top = True
  list_display = ('id', 'title' , 'category', 'location')
  inlines = [AdImageInline]


class DiscountAdmin(admin.ModelAdmin):
  save_on_top = True
  list_display = ('id', 'title', 'price', 'date_to', 'link')

class NewsAdmin(admin.ModelAdmin):
  save_on_top = True
  list_display = ('id', 'title', 'create_date', 'description')
 

admin.site.register(Category, CategoryAdmin) 
admin.site.register(Subcategory, SubcategoryAdmin)
admin.site.register(Advertisement, AdvertisementAdmin) 
admin.site.register(Discount, DiscountAdmin)
admin.site.register(News, NewsAdmin)