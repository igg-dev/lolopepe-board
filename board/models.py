# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.db import models
from unidecode import unidecode
from django.template.defaultfilters import slugify
from multiselectfield import MultiSelectField
import datetime
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
class Category(models.Model):
	  name = models.TextField( _(u'название'), max_length=255)
	  slug = models.SlugField(max_length=50, unique=True, blank=True, default=None, null=True)
	  
	  def __unicode__(self):
	  	  return unicode(self.name)
	  
	  def save(self):
	      self.slug = slugify(unidecode(self.name))
	      super(Category, self).save()

	  class Meta:
		    verbose_name = _(u'Категория')
		    verbose_name_plural = _(u'Категории')

class Subcategory(models.Model):
	  name = models.TextField( _(u'Название'), max_length=255)
	  category = models.ForeignKey(Category, related_name='subcategories', on_delete=models.CASCADE)
	  parentId = models.ForeignKey("self", related_name='children',default=None, blank=True, null=True)
	  slug = models.SlugField(max_length=50, unique=True, blank=True, default=None, null=True)

	  def __unicode__(self):
	  	  return unicode(self.name)
	  
	  def save(self):
	      self.slug = slugify(unidecode(self.name))
	      super(Subcategory, self).save()

	  class Meta:
		    verbose_name = _(u'Подкатегория')
		    verbose_name_plural = _(u'Подкатегории')

# class Document(models.Model):
#     description = models.CharField(max_length=255, blank=True)
#     image = models.ImageField(upload_to='images/')
#     uploaded_at = models.DateTimeField(auto_now_add=True)

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
  

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    if hasattr(instance, 'profile'):
        instance.profile.save()


class Advertisement(models.Model):
	  ALL_KG = 'All_Kyrgyzstan'
	  ALA_BUKA = 'Ala_Buka'
	  ANANEVO = 'Ananevo'
	  ARAVAN = 'Aravan'
	  AT_BASHI = 'At_Bashi'
	  BAET = 'Baet'
	  BAETOV = 'Baetov'
	  BAZAR_KORGON = 'Bazar_Korgon'
	  BAKAY_ATA = 'Bakay_Ata'
	  BAKTUU_DOLONOTU = 'Baktuu dolonotu'
	  BALYIKCHI = 'Balyikchi'
	  BATKEN = 'Batken'
	  BELOVODSKOE = 'Belovodskoe'
	  BISHKEK = 'Bishkek'
	  BOKONBAEVO = 'Bokonbaevo'
	  BOSTERI = 'Bosteri'
	  GRIGOREVKA = 'Grigorevka'
	  GULCHA = 'Gulcha'
	  DAROOT_KORGON = 'Daroot_Korgon'
	  DZHALAL_ABAD = 'Dzhalal_Abad'
	  ZHARKYINBAEV = 'Zharkyinbaev'
	  ISFANA = 'Isfana'
	  KAZARMAN = 'Kazarman'
	  KAINDYI = 'Kaindyi'
	  KANISH_KIYA = 'Kanish_Kiya'
	  KANT = 'Kant'
	  KARA_BALTA = 'Kara_Balta'
	  KARAKOL = 'Karakol'
	  KARA_KUL = 'Kara_kul'
	  KARA_KULDZHA = 'Kara_Kuldzha'
	  KARA_OY = 'Kara_Oy'
	  KARA_SUU = 'Kara_Suu'
	  KASHAT = 'Kashat'
	  KEMIN = 'Kemin'
	  KERBEN = 'Kerben'
	  KOZHOYAR = 'Kozhoyar'
	  KOK_OY = 'Kok_Oy'
	  KORUMDU = 'Korumdu'
	  KOCHKOR = 'Kochkor'
	  KYIZYIL_ADYIR = 'Kyizyil_Adyir'
	  KYIZYIL_KIYA = 'Kyizyil_Kiya'
	  KYIZYIL_SUU = 'Kyizyil_Suu'
	  LEBEDINOVKA = 'Lebedinovka'
	  MASSYI = 'Massyi'
	  NARYIN = 'Naryin'
	  NOOKAT = 'Nookat'
	  ORTO_ORUKTU = 'Orto_Oruktu'
	  OSH = 'Osh'
	  POKROVKA = 'Pokrovka'
	  PULGON = 'Pulgon'
	  SEMENOVKA = 'Semenovka'
	  SOKULUK = 'Sokuluk'
	  SUZAK = 'Suzak'
	  SULYUKTA = 'Sulyukta'
	  TALAS = 'Talas'
	  TAMCHYI = 'Tamchyi'
	  TEMIR = 'Temir'
	  TEPLOKLYUCHENKA = 'Teploklyuchenka'
	  TOKMAK = 'Tokmak'
	  TOKTOGUL = 'Toktogul'
	  TYUP = 'Tyup'
	  UZGEN = 'Uzgen'
	  CHAEK = 'Chaek'
	  CHOK_TAL = 'Chok_Tal'
	  CHOLPON_ATA = 'Cholpon_Ata'
	  CHON_SARYI_OY = 'Chon_Saryi_Oy'
	  SHOPOKOV = 'Shopokov'

	  LOCATIONS = (
	  	(ALL_KG,'Весь Кыргызстан'), (ALA_BUKA,'Ала-Бука'),	(ANANEVO,'Ананьево'),	(ARAVAN,'Араван'),	(AT_BASHI,'Ат-Баши'),	(BAET,'Бает'),	(BAETOV,'Баетов'),	(BAZAR_KORGON,'Базар-Коргон'),	(BAKAY_ATA,'Бакай-Ата'),	(BAKTUU_DOLONOTU,'Бактуу долоноту'),	(BALYIKCHI,'Балыкчи'),	(BATKEN,'Баткен'),	(BELOVODSKOE,'Беловодское'),	(BISHKEK,'Бишкек'),	(BOKONBAEVO,'Боконбаево'),	(BOSTERI,'Бостери'),	(GRIGOREVKA,'Григорьевка'),	(GULCHA,'Гульча'),	(DAROOT_KORGON,'Дароот-Коргон'),	(DZHALAL_ABAD,'Джалал-Абад'),	(ZHARKYINBAEV,'Жаркынбаев'),	(ISFANA,'Исфана'),	(KAZARMAN,'Казарман'),	(KAINDYI,'Каинды'),	(KANISH_KIYA,'Каниш-Кия'),	(KANT,'Кант'),	(KARA_BALTA,'Кара-Балта'),	(KARAKOL,'Каракол'),	(KARA_KUL,'Кара-куль'),	(KARA_KULDZHA,'Кара-Кульджа'),	(KARA_OY,'Кара-Ой'),	(KARA_SUU,'Кара-Суу'),	(KASHAT,'Кашат'),	(KEMIN,'Кемин'),	(KERBEN,'Кербен'),	(KOZHOYAR,'Кожояр'),	(KOK_OY,'Кок-Ой'),	(KORUMDU,'Корумду'),	(KOCHKOR,'Кочкор'),	(KYIZYIL_ADYIR,'Кызыл-Адыр'),	(KYIZYIL_KIYA,'Кызыл-Кия'),	(KYIZYIL_SUU,'Кызыл-Суу'),	(LEBEDINOVKA,'Лебединовка'),	(MASSYI,'Массы'),	(NARYIN,'Нарын'),	(NOOKAT,'Ноокат'),	(ORTO_ORUKTU,'Орто-Орукту'),	(OSH,'Ош'),	(POKROVKA,'Покровка'),	(PULGON,'Пульгон'),	(SEMENOVKA,'Семеновка'),	(SOKULUK,'Сокулук'),	(SUZAK,'Сузак'),	(SULYUKTA,'Сулюкта'),	(TALAS,'Талас'),	(TAMCHYI,'Тамчы'),	(TEMIR,'Темир'),	(TEPLOKLYUCHENKA,'Теплоключенка'),	(TOKMAK,'Токмак'),	(TOKTOGUL,'Токтогул'),	(TYUP,'Тюп'),	(UZGEN,'Узген'),	(CHAEK,'Чаек'),	(CHOK_TAL,'Чок-Тал'),	(CHOLPON_ATA,'Чолпон-Ата'),	(CHON_SARYI_OY,'Чон-Сары-Ой'),	(SHOPOKOV,'Шопоков'),
	 	)


	  KGS = "KGS"
	  USD = "USD"
	  UNITS = (
	    (KGS,'KGS'),	(USD,'USD')
	  )

	  location = models.CharField(_(u'Местоположение'), max_length=30, choices = LOCATIONS, default = BISHKEK)
	  category = models.ForeignKey(Category, on_delete=models.CASCADE)
	  subcategory = models.ForeignKey(Subcategory, on_delete=models.CASCADE)
	  title = models.CharField(_(u'Заголовок'), max_length=255, null=False)
	  description = models.TextField(_(u'Описание'), max_length=2048, null=False)
	  price = models.CharField(_(u'Цена'), max_length=100, default="Договорная", blank=True, null=True)
	  unit = models.CharField(_(u'Валюта'), max_length=10, choices = UNITS, default = KGS)
	  # create_date = models.DateTimeField(default=None, blank=True, null=True)
	  create_date = models.DateTimeField(auto_now_add=True)
	  author = models.ForeignKey(Profile, on_delete=models.CASCADE)
	  # image = models.ImageField(upload_to='images/')

	  def save(self):
	  		self.create_date = datetime.datetime.now()
	  		super(Advertisement, self).save()		

	  class Meta:
		  	verbose_name = _(u'Объявление')
		  	verbose_name_plural = _(u'Объявления')


class AdImage(models.Model):
		file = models.FileField(upload_to='board_images/', default='/static/board/images/images.png')
		uploaded_at = models.DateTimeField(auto_now_add=True )
		advertisement = models.ForeignKey(Advertisement, related_name='images', on_delete = models.CASCADE)


class Comment(models.Model):
	  adv = models.ForeignKey(Advertisement, on_delete=models.CASCADE)
	  comment = models.TextField(_(u'Комментарий'), max_length=1024, null=False)


class Discount(models.Model):  
	  title = models.CharField(_(u'Название'), max_length=255, null=False)
	  price = models.CharField(_(u'Цена или скидка'), max_length=100, default="Договорная", blank=True, null=True)
	  # create_date = models.DateTimeField(default=None, blank=True, null=True)
	  date_to = models.DateTimeField(_(u'Дата истечения скидки'), blank=True, null=True)
	  link = models.CharField(_(u'Ссылка'), max_length=300, blank=True, null=True)
	  # image = models.ImageField(upload_to='images/')

	  def save(self):
	  		self.create_date = datetime.datetime.now()
	  		super(Discount, self).save()		

	  class Meta:
		  	verbose_name = _(u'Скидка')
		  	verbose_name_plural = _(u'Скидки')

class News(models.Model):  
	  title = models.CharField(_(u'Название'), max_length=255, null=False)
	  description = models.TextField(_(u'Описание'), max_length=4096, null=False)
	  create_date = models.DateTimeField(auto_now_add=True)
	  # image = models.ImageField(upload_to='images/')

	  def save(self):
	  		super(News, self).save()		

	  class Meta:
		  	verbose_name = _(u'Новость')
		  	verbose_name_plural = _(u'Новости')

