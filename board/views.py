from board.models import *
from collections import defaultdict
import datetime
from django.shortcuts import render, redirect, get_object_or_404
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from .forms import AdvertisementForm, ImageForm, SignUpForm, CommentForm
from django.db import transaction
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.forms import modelformset_factory


def home_page(request):
    ads = Advertisement.objects.all()
    categories = Category.objects.all()
    subcategories = Subcategory.objects.all()
    subcategories_grouped = defaultdict(list)
    # images = AdImage.objects.all()
    # for im in images:
    #     print im.advertisement.pk
    #     print im.file.url

    for subcategory in subcategories:
        subcategories_grouped[subcategory.category_id].append(subcategory)
        
    subcategories_by_category= dict(subcategories_grouped)

    discounts = Discount.objects.all()

    return render(request, 'board/home.html', {'ads': ads, 'categories': categories, 'subcategories_by_category': subcategories_by_category, 'discounts': discounts})


def ad_show(request, id):
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            form.save()
            id= form.cleaned_data['adv']
            ad = get_object_or_404(Advertisement.objects, pk = id)
            return redirect(ad)
    else:
        ad = get_object_or_404(Advertisement.objects, pk = id)
        comments = Comment.objects.filter(adv_id= ad.id)
        comment_form = CommentForm()
    return render(request, 'board/ad_show.html', {'ad': ad, 'comments': comments, 'form': comment_form})


def contact_page(request):
    return render(request, 'board/contacts.html')


def ad_new(request):
    return render(request, 'board/ad_new.html')

@login_required(login_url='/login/')
def add_advertisement(request):
    ImageFormSet = modelformset_factory(AdImage, form=ImageForm)
    if request.method == "POST":
        adv_form = AdvertisementForm(request.POST, prefix='adv' )
        formset = ImageFormSet(request.POST, request.FILES,
                                       queryset=AdImage.objects.none())
        # image_form = ImageForm(request.POST, request.FILES, prefix='img' )
        if adv_form.is_valid() and formset.is_valid() :
            adv = adv_form.save(commit=False)
            adv.author = request.user.profile
            adv.save()
            for form in formset.cleaned_data:
                          file = form['file']
                          image = AdImage(advertisement=adv, file=file)
                          image.save()
            return redirect('my_profile')
    else:
        adv_form = AdvertisementForm(prefix='adv')
        formset = ImageFormSet(queryset=AdImage.objects.none())
        # image_form = ImageForm(prefix='img')
    return render(request, 'board/adv_add.html', {'adv_form': adv_form, 'formset': formset })

@login_required(login_url='/login/')
def edit_adv(request, pk):
    adv = get_object_or_404(Advertisement, pk=pk)
    if request.method == "POST":
        adv_form = AdvertisementForm(request.POST, prefix='adv', instance=adv )
        image_form = ImageForm(request.POST, request.FILES, prefix='img' )
        if adv_form.is_valid() and image_form.is_valid() :
            adv = form.save(commit=False)
            adv.save()
            return redirect('profile')
    else:
        adv_form = AdvertisementForm(request.POST, prefix='adv', instance=adv )
        image_form = ImageForm(request.POST, request.FILES, prefix='img' )
    return render(request, 'board/adv_edit.html', {'adv_form': adv_form, 'image_form': image_form })

def usr_registration(request):
    if request.method == 'POST':
        user_form = SignUpForm(request.POST)
        if user_form.is_valid():
            user = user_form.save()
          
            raw_password = user_form.cleaned_data.get('password1')
            user_ = authenticate(username=user.username, password=raw_password)
            login(request, user_)
            return redirect('home_page')
        else:
            errors = user_form.errors
            return render(request, 'board/registration.html', {
         'errors':  errors,      
        'user_form': user_form
    })

    else:
        user_form = SignUpForm()

    return render(request, 'board/registration.html', {
        'user_form': user_form
    })


def login_usr(request):
    # if request.user.is_authenticated:
    #     # return redirect('board_index')
    #     return render(request, 'board/board_index.html', {})
    # else:
        logout(request)
        # pdb.set_trace()
        username = password = ''
        if request.POST:
            username = request.POST['username']
            password = request.POST['password']

            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return redirect('home_page')
        return render(request, 'board/login_usr.html', {})


@login_required(login_url='/login/')
def my_profile(request):
    # ads = Advertisement.objects.all()
    categories = Category.objects.all()
    subcategories = Subcategory.objects.all()
    # user = Profile.objects.all()
    subcategories_grouped = defaultdict(list)

    for subcategory in subcategories:
        subcategories_grouped[subcategory.category_id].append(subcategory)
        
    subcategories_by_category= dict(subcategories_grouped)

    my_ads = Advertisement.objects.filter(author = request.user.profile)
    return render(request, 'board/my_profile.html', { 'categories': categories, 'subcategories_by_category': subcategories_by_category, 'user': 0, 'my_ads': my_ads})


def logout_view(request):
    logout(request)
    return redirect('home_page')