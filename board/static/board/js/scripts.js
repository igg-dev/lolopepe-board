$=jQuery;

$( document ).ready(function() {
    $('.category-link').click(function(e)
    {
         e.preventDefault();
         var par = $(this).parents('.mobile-dropdown-categories')[0]
         $(par).find('ul.cat-list').slideToggle("fast");
     }
 )
    $("#content-slider").lightSlider({
                loop:true,
                keyPress:true
            });
            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:9,
                slideMargin: 0,
                speed:500,
                auto:true,
                loop:true,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }  
            });
            var price;
            $('.price .count').text(price)
            if (price=='Договорная'){
                $('.price .unit').text('')
            };
            $(".tab-bar button").click(function(){
                   $(".tab-bar button").addClass("active");
            });
            $.validate({
       lang : 'ru'
    });

  $.validate({
  modules : 'security',
  onModulesLoaded : function() {
    var optionalConfig = {
      fontSize: '12pt',
      padding: '4px',
      bad : 'Очень слабый',
      weak : 'Слабый',
      good : 'Хороший',
      strong : 'Сильный'
    };

    $('input[name="password1"]').displayPasswordStrength(optionalConfig);
  }

  // form : '#registration-form',
  // onValidate : function($form) {
  //     return {
  //       element : $('#username'),
  //       message : 'Поле может иметь в себе латинские буквы, цифры и должно быть в диапазоне от 4 до 20 символов'
  //     }
  //   },
  });

    function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
    
    // Loop through the FileList and render image files as thumbnails.
    for (var i = 0, f; f = files[i];  i++ ) {
      // Only process image files.
      if(i>11){
          break;
        }
      if (!f.type.match('image.*')) {
        continue;
      
      }
      // FileList : {
      //   0,
      //   1,
      //   2,
      //   length: 12,
      //    item
      // }
      var reader = new FileReader();

      reader.onload = (function(theFile) {
        return function(e) {
          // Render thumbnail.
          var span = document.createElement('span');
          span.innerHTML = ['<img class="thumb" src="', e.target.result,
                            '" title="', escape(theFile.name), '"/>'].join('');
          document.getElementById('list').insertBefore(span, null);
        };
      })(f);

      reader.readAsDataURL(f);
    }
  }
  document.getElementById('id_form-0-file').addEventListener('change', handleFileSelect, false);
});

function openTab(tabName) {
    var i;
    var x = document.getElementsByClassName("tabs");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    document.getElementById(tabName).style.display = "block";  
}
