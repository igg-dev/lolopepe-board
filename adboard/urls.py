from django.conf.urls import url
from django.contrib import admin
from board import views
from django.core.files import File
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', views.home_page, name='home_page'),
    url(r'^ads/add/$', views.add_advertisement, name='add_adv'),
    url(r'^login/$', views.login_usr, name='usr_login'),
    url(r'^ads/(?P<id>[0-9]*)/$', views.ad_show, name="show_ad"),  
    url(r'^contacts/', views.contact_page, name='contact_page'), 
    url(r'^registration/', views.usr_registration, name='registration_page'),
    url(r'^ad_new/', views.ad_new, name='ad_new'),
    url(r'^profile/', views.my_profile, name='my_profile'),
    url(r'^logout/$', views.logout_view, name='logout'),
    # url(r'^ad_show_submit/', views.ad_show_submit, name='ad_show_submit')
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
