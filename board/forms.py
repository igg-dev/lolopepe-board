# -*- coding: utf-8 -*-

from django import forms
from .models import Advertisement, AdImage, Comment
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class AdvertisementForm(forms.ModelForm):

    class Meta:
        model = Advertisement
        fields = ('location', 'category', 'subcategory', 'title', 'description', 'price', 'unit', )

class ImageForm(forms.ModelForm):
    file = forms.FileField(label='')

    class Meta:
        model = AdImage
        fields = ('file', )

class SignUpForm(UserCreationForm):

    class Meta:
        model = User
        fields = ('username', 'password1', 'password2', )


class CommentForm(forms.ModelForm):
    comment =  forms.CharField ( widget=forms.widgets.Textarea() )

    class Meta():
    	model = Comment
    	fields = ()


